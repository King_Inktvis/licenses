use crate::javascript::javascript_license_overview;
use crate::rust::rust_license_overview;
use clap::StructOpt;
use serde::Serialize;
use std::collections::HashMap;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::str::FromStr;

mod display;
mod error;
mod javascript;
mod rust;
mod yarn;

#[derive(Debug, StructOpt)]
#[structopt(name = "licenses", about)]
pub struct Opt {
    /// The value used for grouping the results (name, license).
    #[structopt(short, long, default_value = "name")]
    grouping: GroupArgument,
    /// Search for licenses containing the given keyword.
    #[structopt(short, long)]
    search: Option<String>,
    /// The format to represent the data in (text, json, yaml).
    #[structopt(short, long, default_value = "text")]
    format: Format,
    /// File location to save the resulting output, when not specified it is send to stdout instead.
    #[structopt(short, long)]
    output: Option<PathBuf>,
    /// Location of the project of you want to see the dependencies.
    #[structopt(parse(from_os_str), default_value = "")]
    project_folder: PathBuf,
}

#[derive(Debug)]
enum GroupArgument {
    Name,
    License,
}

impl FromStr for GroupArgument {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "name" => Ok(GroupArgument::Name),
            "license" => Ok(GroupArgument::License),
            _ => Err("Invalid grouping value given"),
        }
    }
}

#[derive(Debug)]
enum Format {
    Text,
    Json,
    Yaml,
}

impl FromStr for Format {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "text" => Ok(Format::Text),
            "json" => Ok(Format::Json),
            "yaml" => Ok(Format::Yaml),
            _ => Err("Invalid output value given"),
        }
    }
}

#[derive(Serialize, Debug)]
pub struct LicensedUnder {
    license: String,
    names: Vec<String>,
}

#[derive(Serialize, Debug)]
pub struct Data {
    name: String,
    license: String,
}

pub enum Grouping {
    Name(Vec<Data>),
    License(Vec<LicensedUnder>),
}

fn main() {
    let opt: Opt = clap::Parser::parse();
    let mut list = Vec::new();
    if let Ok(mut overview) = rust_license_overview(&opt.project_folder) {
        list.append(&mut overview)
    }
    if let Ok(mut overview) = javascript_license_overview(&opt.project_folder) {
        list.append(&mut overview)
    }
    if let Some(find) = &opt.search {
        let find = find.to_lowercase();
        list = list
            .into_iter()
            .filter(|entry| entry.license.to_lowercase().contains(&find))
            .collect();
    }
    let out = grouped_output(&opt, list);
    let text = match opt.format {
        Format::Text => out.text(opt.output.is_none()),
        Format::Json => out.json(),
        Format::Yaml => out.yaml(),
    };
    match opt.output {
        Some(output) => {
            let mut f = File::create(output).unwrap();
            f.write_all(text.as_bytes()).unwrap();
        }
        None => {
            println!("{}", text);
        }
    }
}

fn grouped_output(opt: &Opt, list: Vec<Data>) -> Grouping {
    let out = match opt.grouping {
        GroupArgument::License => {
            let mut grouped = Box::new(HashMap::new());
            for i in list.iter() {
                let entry = grouped
                    .entry(i.license.to_string())
                    .or_insert_with(Vec::new);
                entry.push(i.name.to_string());
            }
            let ret = grouped
                .into_iter()
                .map(|(license, names)| LicensedUnder { license, names })
                .collect();
            Grouping::License(ret)
        }
        GroupArgument::Name => Grouping::Name(list),
    };
    out
}
