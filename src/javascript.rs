use crate::error::ErrorStates;
use crate::Data;
use colored::Colorize;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::ffi::OsString;
use std::fs::{read_dir, DirEntry, File};
use std::io::Read;
use std::path::{Path, PathBuf};
use yaml_rust::yaml::Hash;
use yaml_rust::{Yaml, YamlLoader};

#[derive(Serialize, Deserialize, Debug)]
struct Json {
    license: String,
}

pub fn javascript_license_overview(project_folder: &Path) -> Result<Vec<Data>, ErrorStates> {
    let mut res = Vec::new();
    if let Ok(mut yarn) = get_yarn_v2(project_folder) {
        res.append(&mut yarn);
    } else if let Ok(mut yarn) = get_yarn_v1(project_folder) {
        res.append(&mut yarn);
    } else if let Ok(mut npm) = get_npm(project_folder) {
        res.append(&mut npm);
    }
    Ok(res)
}

fn get_yarn_v1(project_folder: &Path) -> Result<Vec<Data>, ErrorStates> {
    let mut loc_file = project_folder.to_path_buf();
    loc_file.push("yarn.lock");
    let mut f = File::open(loc_file)?;
    let mut buf = String::new();
    f.read_to_string(&mut buf)?;
    let lock = yarn_lock_parser::parse_str(&buf)?;
    let dependency_names: Vec<String> = lock.into_iter().map(|e| e.name.into()).collect();
    build_node_modules_license_list(project_folder, dependency_names)
}

fn get_yarn_v2(project_folder: &Path) -> Result<Vec<Data>, ErrorStates> {
    let mut overview: HashMap<String, String> = HashMap::new();
    let doc = load_yarn_lock_file(project_folder)?;
    let dir = dir_list(project_folder);
    for (key, value) in doc.iter() {
        get_dependency_license(project_folder, &dir, &mut overview, key, value)
    }
    Ok(overview
        .into_iter()
        .map(|(name, license)| Data { name, license })
        .collect())
}

fn load_yarn_lock_file(project_folder: &Path) -> Result<Hash, ErrorStates> {
    let mut loc_file = project_folder.to_path_buf();
    loc_file.push("yarn.lock");
    let mut f = File::open(loc_file)?;
    let mut buf = String::new();
    f.read_to_string(&mut buf)?;
    let docs = YamlLoader::load_from_str(&buf)?;
    let doc = docs[0].as_hash().unwrap();
    Ok(doc.clone())
}

fn get_dependency_license(
    project_folder: &Path,
    dir: &[DirEntry],
    overview: &mut HashMap<String, String>,
    key: &Yaml,
    value: &Yaml,
) {
    if let Some(name) = value["resolution"].as_str() {
        if let Some(found) = name_resolution(name, dir) {
            let base_name = name.split("@npm").next().unwrap();
            let mut dep_loc = project_folder.to_path_buf();
            dep_loc.push(".yarn/cache");
            dep_loc.push(found);
            let zipfile = File::open(dep_loc).unwrap();
            let mut archive = zip::ZipArchive::new(zipfile).unwrap();
            let tmp = archive.by_name(&format!("node_modules/{}/package.json", base_name));
            match tmp {
                Ok(mut file) => {
                    let license = get_license(&mut file);
                    overview.insert(base_name.into(), license);
                }
                Err(_) => {
                    println!(
                        "{}",
                        format!("Could not find package.json for: {}", base_name).magenta()
                    );
                }
            };
        }
    } else {
        println!(
            "{}",
            format!("Resolution not found for: {:?}", key.as_str()).cyan()
        );
    }
}

fn get_license(file: &mut dyn Read) -> String {
    let mut contents = Vec::new();
    file.read_to_end(&mut contents).unwrap();
    let res = serde_json::from_slice::<Json>(&contents);
    match res {
        Ok(x) => x.license,
        Err(_) => "No license present".into(),
    }
}

fn dir_list(root: &Path) -> Vec<DirEntry> {
    let mut loc = PathBuf::from(root);
    loc.push(".yarn/cache");
    let dir = read_dir(loc).unwrap();
    let list: Vec<DirEntry> = dir.map(|e| e.unwrap()).collect();
    list
}

fn name_resolution(resolution: &str, files: &[DirEntry]) -> Option<OsString> {
    let mut name = String::from(resolution);
    name = name.replace("/", "-");
    name = name.replace("@npm:", "-npm-");
    for f in files.iter() {
        if f.file_name().as_os_str().to_str().unwrap().contains(&name) {
            return Some(f.file_name());
        }
    }
    None
}

fn get_npm(project_folder: &Path) -> Result<Vec<Data>, ErrorStates> {
    let lock = load_npm_lock_file(project_folder)?;
    let dependency_names: Vec<String> = lock.dependencies.into_iter().map(|e| e.0).collect();
    build_node_modules_license_list(project_folder, dependency_names)
}

fn build_node_modules_license_list(
    project_folder: &Path,
    dependency_names: Vec<String>,
) -> Result<Vec<Data>, ErrorStates> {
    let mut overview: HashMap<String, String> = HashMap::new();
    for key in dependency_names.into_iter() {
        let file_loc = get_node_modules_path(project_folder, &key);
        let file = File::open(file_loc);
        match file {
            Ok(mut file) => {
                let license = get_license(&mut file);
                overview.insert(key, license);
            }
            Err(_) => {
                println!("package file not found for {}", key)
            }
        };
    }
    Ok(overview
        .into_iter()
        .map(|(name, license)| Data { name, license })
        .collect())
}

fn get_node_modules_path(project_folder: &Path, name: &str) -> PathBuf {
    let mut file_loc = project_folder.to_path_buf();
    file_loc.push("node_modules");
    file_loc.push(&name);
    file_loc.push("package.json");
    file_loc
}

#[derive(Debug, Deserialize)]
struct PackageLock {
    dependencies: HashMap<String, serde_json::Value>,
}

fn load_npm_lock_file(project_folder: &Path) -> Result<PackageLock, ErrorStates> {
    let mut loc_file = project_folder.to_path_buf();
    loc_file.push("package-lock.json");
    let mut f = File::open(loc_file)?;
    let mut buf = Vec::new();
    f.read_to_end(&mut buf)?;
    let lock: PackageLock = serde_json::from_slice(&buf)?;
    Ok(lock)
}
