use colored::Colorize;
use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use yaml_rust::ScanError;
use yarn_lock_parser::YarnLockError;

#[derive(Debug)]
pub enum ErrorStates {
    Missing,
    Parsing,
}

impl Display for ErrorStates {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Error for ErrorStates {}

impl From<std::io::Error> for ErrorStates {
    fn from(e: std::io::Error) -> Self {
        print(&e);
        ErrorStates::Missing
    }
}

impl From<toml::de::Error> for ErrorStates {
    fn from(e: toml::de::Error) -> Self {
        print(&e);
        ErrorStates::Parsing
    }
}

impl From<serde_yaml::Error> for ErrorStates {
    fn from(e: serde_yaml::Error) -> Self {
        print(&e);
        ErrorStates::Parsing
    }
}

impl From<ScanError> for ErrorStates {
    fn from(e: ScanError) -> Self {
        print(&e);
        ErrorStates::Parsing
    }
}

impl From<serde_json::Error> for ErrorStates {
    fn from(e: serde_json::Error) -> Self {
        print(&e);
        ErrorStates::Parsing
    }
}

impl From<yarn_lock_parser::YarnLockError> for ErrorStates {
    fn from(e: YarnLockError) -> Self {
        print(&e);
        ErrorStates::Parsing
    }
}

fn print(e: &dyn Error) {
    if cfg!(debug_assertions) {
        println!("{}", format!("{:?}", e).red());
    }
}
