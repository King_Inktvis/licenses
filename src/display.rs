use crate::{Data, Grouping, LicensedUnder};
use colored::*;

impl Grouping {
    pub fn text(&self, color: bool) -> String {
        match self {
            Grouping::Name(list) => data_to_text(color, list),
            Grouping::License(list) => license_grouped_to_text(color, list),
        }
    }

    pub fn json(&self) -> String {
        match self {
            Grouping::Name(s) => serde_json::to_string_pretty(s).unwrap(),
            Grouping::License(s) => serde_json::to_string_pretty(s).unwrap(),
        }
    }

    pub fn yaml(&self) -> String {
        match self {
            Grouping::Name(s) => serde_yaml::to_string(s).unwrap(),
            Grouping::License(s) => serde_yaml::to_string(s).unwrap(),
        }
    }
}

fn license_grouped_to_text(color: bool, list: &[LicensedUnder]) -> String {
    list.iter()
        .map(|entry| {
            let mut name_string = String::new();
            for n in entry.names.iter() {
                name_string.push_str(n);
                name_string.push(' ');
            }
            if color {
                format!("{} {}", entry.license.blue(), name_string.trim().green())
            } else {
                format!("{} {}", entry.license, name_string.trim())
            }
        })
        .collect::<Vec<String>>()
        .join("\n")
}

fn data_to_text(color: bool, list: &[Data]) -> String {
    list.iter()
        .map(|i| {
            if color {
                format!("{} {}", i.name.green(), i.license.blue())
            } else {
                format!("{} {}", i.name, i.license)
            }
        })
        .collect::<Vec<String>>()
        .join("\n")
}
