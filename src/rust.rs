use crate::error::ErrorStates;
use crate::Data;
use serde::Deserialize;
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::Read;
use std::path::Path;

#[derive(Deserialize)]
struct TomlFile {
    package: Package,
}

#[derive(Deserialize)]
struct Package {
    name: String,
    license: Option<String>,
}

#[derive(Deserialize)]
struct Lockfile {
    package: Vec<LockEntry>,
}

#[derive(Deserialize, Debug)]
struct LockEntry {
    name: String,
    version: String,
}

pub fn rust_license_overview(project_folder: &Path) -> Result<Vec<Data>, ErrorStates> {
    let name = get_name(project_folder)?;
    let mut overview: HashMap<String, String> = HashMap::new();
    let mut loc_file = project_folder.to_path_buf();
    loc_file.push("Cargo.lock");
    let mut f = File::open(loc_file)?;

    let mut buf: Vec<u8> = Vec::new();
    f.read_to_end(&mut buf)?;
    let data: Lockfile = toml::from_slice(&buf)?;
    for i in data.package.into_iter() {
        if i.name != name && !overview.contains_key(&i.name) {
            let l = get_license(&i.name, &i.version);
            overview.insert(i.name, l);
        }
    }
    Ok(overview
        .into_iter()
        .map(|(name, license)| Data { name, license })
        .collect())
}

fn get_license(name: &str, version: &str) -> String {
    let home = env::var("HOME").unwrap();
    let loc = format!(
        "{}/.cargo/registry/src/github.com-1ecc6299db9ec823/{}-{}/Cargo.toml",
        home, name, version
    );
    match File::open(&loc) {
        Ok(mut f) => {
            let mut buf: Vec<u8> = Vec::new();
            f.read_to_end(&mut buf).unwrap();
            let data: TomlFile = toml::from_slice(&buf).unwrap();
            match data.package.license {
                Some(l) => l,
                None => "No license specified".into(),
            }
        }
        Err(e) => {
            println!("{} {}", loc, e);
            "Files not found".into()
        }
    }
}

fn get_name(project_folder: &Path) -> Result<String, ErrorStates> {
    let mut file = project_folder.to_path_buf();
    file.push("Cargo.toml");
    let mut buf: Vec<u8> = Vec::new();
    let mut f = File::open(file)?;

    f.read_to_end(&mut buf).unwrap();
    let data: TomlFile = toml::from_slice(&buf).unwrap();
    Ok(data.package.name)
}
